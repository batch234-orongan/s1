package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;

@RestController
// enable cross origin request via @CrossOrigin
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Get a post
    @GetMapping("/posts")
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }
    // update post
    @RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postid, stringToken, post);
    }
    // delete posts
    @RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value ="Authorization") String stringToken){
        return postService.deletePost(postid, stringToken);
    }
    // retrieve authors post
    @GetMapping("/myPosts")
    public ResponseEntity<Object> getUsersPosts(@RequestHeader(value= "Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getUsersPosts(stringToken), HttpStatus.OK);
    }


}
