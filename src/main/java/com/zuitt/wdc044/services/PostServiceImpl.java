package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;


    public void createPost(String stringToken, Post post) {
        // We use the findByUsername method to find the user.
        // We can retrieve the username in the payload of the jwt.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        // Title and content will come from the req.body
        Post newPost = new Post();
        newPost.setTittle(post.getTittle());
        newPost.setContent(post.getContent());
        // author will come from the decoded jwt payload.
        newPost.setUser(author);
        postRepository.save(newPost);
    }


    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTittle(post.getTittle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }


    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)){
            postRepository.delete(postForDeleting);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>( "You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // get all users post
    public Iterable<Post> getUsersPosts(String stringToken){
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User postAuthor = userRepository.findByUsername(authenticatedUser);
        return postAuthor.getPosts();
    }
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

}
