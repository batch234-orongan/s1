package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // create a post
    void createPost(String stringToken, Post post);
    // retrieve posts
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // delete a post
    ResponseEntity deletePost(Long id, String stringToken);

    // get all post
    Iterable<Post> getPosts();


    // get authors posts
    Iterable<Post> getUsersPosts(String stringToken);
}
