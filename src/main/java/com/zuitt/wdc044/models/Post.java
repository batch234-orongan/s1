package com.zuitt.wdc044.models;

import javax.persistence.*;
// this comes from dependency that we have set up in pom.xml
@Entity
// This class is a representation of a database table via this annotation.
@Table(name= "posts")
// This annotation designates the table name.
public class Post {
    @Id
    // sets up the primary key
        @GeneratedValue
    // auto-increment
    private long id;
    // id will be a long integer

    @Column
    // class properties will be shown in each column for the table.
    private String tittle;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    // constructors
        // default constructors for retrieval
    public Post(){}

        // parameterized constructor
    public Post(String tittle, String content){
        this.tittle = tittle;
        this.content = content;
    }

    // Getters and Setters
    public String getTittle(){
        return tittle;
    }

    public void setTittle(String tittle){
        this.tittle = tittle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser(){ return user; }

    public void setUser(User user){ this.user = user; }
}
