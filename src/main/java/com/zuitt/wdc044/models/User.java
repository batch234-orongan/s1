package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity

@Table(name= "users")

public class User {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    // to avoid the infinite recursion
    @JsonIgnore
    private Set<Post> posts;

    public Set<Post> getPosts(){
        return posts;
    }

    // constructors
    // default constructors for retrieval
    public User(){}

    // parameterized constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // Getters and Setters
    public Long getId(){
        return id;
    }
    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
