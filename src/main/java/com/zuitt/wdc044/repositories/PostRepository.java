package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
// this is responsible for the CRUD operations
public interface PostRepository extends CrudRepository<Post, Object> {

}
